"""ypro URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, re_path
from sheep import views
from sheep.views import loginView, regView, logoutView,updateView

urlpatterns = [
    path('admin/', admin.site.urls),
    path('login/', loginView),#登录
    path('index/', views.index),
    re_path(r'^detail/(?P<goods_id>\d+)/$', views.detailview.as_view(), name="detail"),
    path('form/',views.get_sheeps),
    path('reg/', regView),#注册
    path('logout/', logoutView),#退出登录
    path('update/', updateView),#修改密码
]

