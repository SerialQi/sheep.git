from django.db import models
class Sheeps(models.Model):
    name = models.CharField(max_length=100, verbose_name=u"名称")
    degree = models.CharField(max_length=100, verbose_name=u"喜爱程度")
    sheepdesc = models.CharField(max_length=100, verbose_name=u"角色描述")
    weight = models.CharField(max_length=100, verbose_name=u"单位")
    imgurl=models.ImageField(upload_to="static/upload/%Y/%m", verbose_name=u"角色图片", max_length=100)
    class Meta:
        verbose_name = u"角色"
        verbose_name_plural = verbose_name
    def __str__(self):
        return self.name






