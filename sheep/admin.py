from django.contrib import admin
from sheep.models import Sheeps

class sheepsAdmin(admin.ModelAdmin):
    list_display = ('name', 'degree','sheepdesc','weight')
    list_editable = ('sheepdesc',)
    search_fields = ('name',) 
admin.site.register(Sheeps,sheepsAdmin)  
 
