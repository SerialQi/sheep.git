from django.forms import ModelForm
from sheep.models import Sheeps
class SheepsForm(ModelForm):
    class Meta:
        model = Sheeps
        #fields = ['name','degree']
        #exclude = ['imgurl']
        fields = ['name', 'degree','sheepdesc','weight']
        # fields = '__all__'