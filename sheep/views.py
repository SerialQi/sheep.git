from django.shortcuts import render
from django.core.mail import send_mail
from django.shortcuts import render, redirect
from django.contrib.auth.models import User
from django.contrib.auth import login,logout,authenticate
from random import Random
from django.views import View
from sheep.forms import SheepsForm
from sheep.models import Sheeps

def index(request):
    allsheeps = Sheeps.objects.all()  # 动态读取
    return render(request, 'list.html', {
        "sheepslist": allsheeps,
    })

class detailview(View):
    def get(self, request, goods_id):
        sheeps = Sheeps.objects.get(id=int(goods_id))
        return render(request, "detail.html", {
            "sheeps": sheeps,
        })

#登录
def loginView(request):
    if request.method=="POST":
        username=request.POST.get("username")
        password=request.POST.get("password")
        if User.objects.filter(username=username):
            user=authenticate(username=username,password=password)
            if user:
                if user.is_active:
                    login(request,user)
                return render(request,"list.html")
            else:
                msg="用户名密码错误"
        else:
            msg="用户名不存在"
    return render(request,"login.html",locals()) 


#表单

def get_sheeps(request):
    sheeps=Sheeps.objects.all()
    SheepsForm(sheeps)
    return render(request, 'form.html', {"sheeps":sheeps,"form":SheepsForm})





#注册
def regView(request):
    if request.method=="POST":
        username=request.POST.get("username")
        password=request.POST.get("password")
        email=request.POST.get("email")
        if User.objects.filter(username=username):
            msg="用户名已存在"
        else:
            user=User.objects._create_user(username=username,password=password,email=email)
            user.save()
            msg="注册成功"
    return render(request,"register.html",locals())
#退出登录
def logoutView(request):
    logout(request)
    return render(request,"login.html")

#修改密码
def updateView(request):
    if request.method=="POST":
        username=request.POST.get("name")
        password=request.POST.get("password")
        newpassword=request.POST.get("newpassword")
        #先验证用户名是否正确
        if User.objects.filter(username=username):
            #验证旧密码是否正确
            user=authenticate(username=username,password=password)
            user.set_password(newpassword)
            user.save()
            msg="密码修改成功"
        else:
            msg="用户不存在"
    return render(request,"update.html",locals())






